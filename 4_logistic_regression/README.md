# 逻辑回归



逻辑回归(Logistic Regression, LR)模型其实仅在线性回归的基础上，套用了一个逻辑函数，但也就由于这个逻辑函数，使得逻辑回归模型能够输出类别的概率。逻辑回归的本质是：假设数据服从这个分布，然后使用极大似然估计做参数的估计。

![theory](images/linear_logistic_regression.png)



## 内容

* [线性回归-最小二乘法](1-Least_squares.ipynb)
* [逻辑回归](2-Logistic_regression.ipynb)

* [特征处理 - 降维](3-PCA_and_Logistic_Regression.ipynb)

