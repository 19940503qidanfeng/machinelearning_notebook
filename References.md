# 参考资料
可以自行在下属列表找找到适合自己的学习资料，虽然罗列的比较多，但是个人最好选择一个深入阅读、练习。当练习到一定程度，可以再看看其他的资料，这样弥补单一学习资料可能存在的欠缺。



## 1. 教程、代码

### 1.1 教程

* [《动手学深度学习》- PyTorch版本](https://tangshusen.me/Dive-into-DL-PyTorch/#/)
* [Introduction — Neuromatch Academy: Deep Learning](https://deeplearning.neuromatch.io/tutorials/intro.html)
* [Pytorch 常用代码合集](https://mp.weixin.qq.com/s/wqhpwShaW26CIF9K9_QD0g)
	* 主要包括如下功能：
	* 导入包和版本查询
	* 单张显卡设置以及多张显卡设置
	* 清除显存
	* 张量处理
	* 计算两组数据之间的欧式距离
	* 模型的定义
	* 模型参数量的计算
	* 模型的可视化
	* 将GPU的数据加载到CPU
	* 数据集均方和标准差的处理
	* 模型的训练与测试demo
	* 模型的保存与断点加载

### 1.2 代码

* [《统计学习方法》的代码](https://gitee.com/afishoutis/MachineLearning)
* [《统计学习方法》PyTorch实现](https://github.com/fengdu78/lihang-code)
*  [pytorch-cifar100](https://github.com/weiaicunzai/pytorch-cifar100)  实现ResNet, DenseNet, VGG, GoogleNet, InceptionV3, InceptionV4, Inception-ResNetv2, Xception, Resnet In Resnet, ResNext,ShuffleNet, ShuffleNetv2, MobileNet, MobileNetv2, SqueezeNet, NasNet, Residual Attention Network, SENet, WideResNet
* [Attention:  xmu-xiaoma666/External-Attention-pytorch: Pytorch implementation of various Attention Mechanisms, MLP, Re-parameter, Convolution, which is helpful to further understand papers.⭐⭐⭐ (github.com)](https://github.com/xmu-xiaoma666/External-Attention-pytorch)   注意力机制，多层神经网络，重参数。
* [Python  TheAlgorithms/Python: All Algorithms implemented in Python (github.com)](https://github.com/TheAlgorithms/Python)
* PytTorch 训练手册  https://github.com/zergtant/pytorch-handbook 

## 2. 工具、技巧

* [形象直观了解谷歌大脑新型优化器LAMB](https://www.toutiao.com/i6687162064395305475/)
* [梯度下降方法的视觉解释（动量，AdaGrad，RMSProp，Adam）](https://www.toutiao.com/i6836422484028293640/)
* [5种常用的交叉验证技术，保证评估模型的稳定性](https://m.toutiaocdn.com/i6838062457596936718)
* [The Neural network zoo](https://www.asimovinstitute.org/neural-network-zoo/) : 各种神经网络架构的解释、图示
* 22 个神经网络结构设计/可视化工具
	- https://www.toutiao.com/i6836884346155041292/
- https://github.com/ashishpatel26/Tools-to-Design-or-Visualize-Architecture-of-Neural-Network
	
* CNN 可视化工具 https://m.toutiaocdn.com/group/6822123587156050435
	- https://poloclub.github.io/cnn-explainer/
	- https://github.com/poloclub/cnn-explainer

* 打标签工具
	- [Label Studio](https://labelstud.io/)
		- Demo video https://www.bilibili.com/video/BV1dL41147KE
		- Documents https://labelstud.io/guide/
	- [LabelImg](https://github.com/tzutalin/labelImg)

* 一款图像转卡通的Python项目，超级值得你练手
	- https://www.toutiao.com/a6821299115175969287/
	- https://github.com/minivision-ai/photo2cartoon

* [Awesome Deep Learning Project Ideas](https://github.com/NirantK/awesome-project-ideas)

* [Machine Learning From Scratch](https://github.com/eriklindernoren/ML-From-Scratch)




## 3. 练习
* http://sofasofa.io/competitions.php?type=practice
* https://www.kaggle.com/competitions
* Machine learning project ideas
  * https://data-flair.training/blogs/machine-learning-project-ideas/
  * https://data-flair.training/blogs/deep-learning-project-ideas/
  * https://www.kdnuggets.com/2020/03/20-machine-learning-datasets-project-ideas.html


* Titanic: notebooks/data-science-ipython-notebooks/kaggle/titanic.ipynb
* 使用神经网络解决拼图游戏 https://www.toutiao.com/a6855437347463365133/
* [Sudoku-Solver](https://github.com/shivaverma/Sudoku-Solver)
* Python 小项目 https://github.com/kyclark/tiny_python_projects


## 4. 机器学习方法

### 4.1 经典机器学习方法
* Programming Multiclass Logistic Regression
notebooks/MachineLearningNotebooks/05.%20Logistic%20Regression.ipynb

* Equation for MLP
notebooks/MachineLearningNotebooks/07.%20MLP%20Neural%20Networks.ipynb

* Optimization methods
notebooks/MachineLearningNotebooks/06.%20Optimization.ipynb


* https://github.com/wmpscc/DataMiningNotesAndPractice/blob/master/2.KMeans%E7%AE%97%E6%B3%95%E4%B8%8E%E4%BA%A4%E9%80%9A%E4%BA%8B%E6%95%85%E7%90%86%E8%B5%94%E5%AE%A1%E6%A0%B8%E9%A2%84%E6%B5%8B.md

* evaluation metrics
http://localhost:8889/notebooks/machineLearning/10_digits_classification.ipynb


* model selection and assessment
http://localhost:8889/notebooks/machineLearning/notebooks/01%20-%20Model%20Selection%20and%20Assessment.ipynb


### 4.2 NN
* 神经网络——梯度下降&反向传播 https://blog.csdn.net/skullfang/article/details/78634317
* 零基础入门深度学习(3) - 神经网络和反向传播算法 https://www.zybuluo.com/hanbingtao/note/476663
* 如何直观地解释 backpropagation 算法？ https://www.zhihu.com/question/27239198
* 一文弄懂神经网络中的反向传播法——BackPropagation https://www.cnblogs.com/charlotte77/p/5629865.html

* https://medium.com/@UdacityINDIA/how-to-build-your-first-neural-network-with-python-6819c7f65dbf
* https://enlight.nyc/projects/neural-network/
* https://www.python-course.eu/neural_networks_with_python_numpy.php


### 4.3 k-Means
* [如何使用 Keras 实现无监督聚类](http://m.sohu.com/a/236221126_717210)

### 4.4 AutoEncoder (自编码/非监督学习)
* https://morvanzhou.github.io/tutorials/machine-learning/torch/4-04-autoencoder/
* https://github.com/MorvanZhou/PyTorch-Tutorial/blob/master/tutorial-contents/404_autoencoder.py
* pytorch AutoEncoder 自编码 https://www.jianshu.com/p/f0929f427d03
* Adversarial Autoencoders (with Pytorch) https://blog.paperspace.com/adversarial-autoencoders-with-pytorch/


### 4.4 损失函数，交叉熵
* 简单的交叉熵，你真的懂了吗？ https://zhuanlan.zhihu.com/p/61944055
* 损失函数：交叉熵详解 https://zhuanlan.zhihu.com/p/115277553
* 一文搞懂交叉熵在机器学习中的使用，透彻理解交叉熵背后的直觉 https://blog.csdn.net/tsyccnh/article/details/79163834
* 一文搞懂熵(Entropy),交叉熵(Cross-Entropy) https://zhuanlan.zhihu.com/p/149186719